from django.db import models

# Create your models here.

class ShortUrl(models.Model):
    url = models.CharField(max_length=200)
    random = models.CharField(max_length=200)

    def __str__(self):
        return self.url + " " + self.random

class Contador(models.Model):
    contador = models.IntegerField(default=1)