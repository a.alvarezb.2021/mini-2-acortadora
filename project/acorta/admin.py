from django.contrib import admin
from .models import ShortUrl, Contador

# Register your models here.
admin.site.register(ShortUrl)
admin.site.register(Contador)