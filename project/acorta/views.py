from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from .models import ShortUrl, Contador


# Create your views here.


@csrf_exempt
def index(request):
    """
        Se encarge de gestionar todas las peticiones GET y POST.
        Si le llega un GET / le da las vistas a la vista index.html, es decir, su pagina principal, donde aparece los dos
        formularios, uno para poner una url y otro para acortarla.
        sI le llega un POST / y la url es valida y el campo short no esta vacio, es decir, se ha metido una valor para acortart,
        la url, se anade a las lista de urls y se guarda en la base de datos.
        Si le llega un POST / y la url es valida y el campo short esta vacio, es decir, se le asigna un numero, que empieza
        por 1 y este va incrementando cada vez que no se le da un valor al campo short, manda la pagina principal con la
        lista de urls.
        Si le llega un POST / y la url no es valida, es decir, no se ha metido una url, manda un error 404.
        """
    if request.method == "GET":
        #Devuelvo la pagina principal con los formularios y la lista d urls
        return render(request, 'acorta/index.html', {'urls': ShortUrl.objects.all()})
    elif request.method == "POST":
        #Obtengo los campos de la url y short
        url = request.POST["url"]
        short = request.POST["short"]
        if url: #Si tengo una url
            if url.startswith("http://") or url.startswith("https://"): # Si la url empieza con http o https
                urls_db = ShortUrl.objects.values_list('url', flat=True) #Obtengo la lista de urls de la base de datos
                if url not in urls_db: #Si la url no esta en la base de datos, la agrego y la guardo
                    #Si tengo un objeto contador creado, obtengo el primero, si no, lo creo y lo guardo
                    contador = Contador.objects.first() or Contador.objects.create(contador=1)
                    #Creo el objeto de la url, si tiene campo short se guarda ese valor, si no, guardo el valor del contador
                    url_object = ShortUrl.objects.create(url=url, random=short or str(contador.contador))
                    url_object.save()
                    contador.contador += 1 # Incremento el contador
                    contador.save()
                return render(request, 'acorta/index.html', {'urls': ShortUrl.objects.all()})
            else: #Si la url no empieza con http o https, le agrego https://
                url = "https://" + url
                urls_db = ShortUrl.objects.values_list('url', flat=True)#Obtengo la lista de urls de la base de datos
                if url not in urls_db:#Si la url no esta en la base de datos, la agrego y la guardo
                    # Si tengo un objeto contador creado, obtengo el primero, si no, lo creo y lo guardo
                    contador = Contador.objects.first() or Contador.objects.create(contador=1)
                    # Creo el objeto de la url, si tiene campo short se guarda ese valor, si no, guardo el valor del contador
                    url_object = ShortUrl.objects.create(url=url, random=short or str(contador.contador))
                    url_object.save()
                    contador.contador += 1
                    contador.save()
                return render(request, 'acorta/index.html', {'urls': ShortUrl.objects.all()})
        else: #Si no tengo una url, mando la pagina principal con la lista de urls
            return render(request, 'acorta/index.html', {'urls': ShortUrl.objects.all()})
    else: #Si lleva otro metodo, mando un Mehtod Not Allowed
        raise HttpResponseNotAllowed("Method not allowed")


@csrf_exempt
def redirect(request, random):
    """
    En caso de la perticion sea un GET /algo, redirige a la url con el valor del campo algo.

    """
    if request.method == "GET":

        try:
            urls_ramdon_db = ShortUrl.objects.values_list('random', flat=True)
            if random in urls_ramdon_db:
                return HttpResponseRedirect(ShortUrl.objects.get(random=random).url)
        except ShortUrl.DoesNotExist:
            raise Http404("URL not found")
